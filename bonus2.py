import string
def pendu(solution):
    affichage = ""
    vie = 10
    for l in solution:
        affichage = affichage + "* "
    lettres_trouvees = ""
    while vie>0: 
        print("Mot à deviner : ", affichage)
        proposition = input("proposez une lettre : ")
        if proposition in solution:
            lettres_trouvees = lettres_trouvees + proposition
            affichage = ""
            for x in solution:
                if x in lettres_trouvees:
                    affichage += x + " "
                else:
                    affichage += "* "
            print("-> Bien vu!")
        else:
            vie = vie - 1
            print("-> Nope. Il vous reste", vie, "tentatives")
        if "*" not in affichage:
            print(">>> Gagné! <<<")
            break
        if vie==0:
            print(" ==========Y= ")
        if vie<=1:
            print(" ||/       |  ")
        if vie<=2:
            print(" ||        0  ")
        if vie<=3:
            print(" ||       /|\ ")
        if vie<=4:
            print(" ||       / \ ")
        if vie<=5:                    
            print("/||           ")
        if vie<=6:
            print("==============\n")
        


def run():
    pendu("test")

run()