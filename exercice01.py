a = 1
b = 2

# Votre code ici
c = b
b = a
a = c

def run():
    assert a == 2
    assert b == 1
