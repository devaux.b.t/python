import math
def factorial(number):
    # Votre code ici
    valeur = number
    if (number<0):
        valeur = abs(number)
    if valeur == 0:
        return 1
    else:
        F = 1
        for k in range(2,valeur+1):
            F = F * k
        if (number<0):
            F = -F
        return F

def run():
    assert factorial(1) == 1
    assert factorial(2) == 2
    assert factorial(3) == 6
    assert factorial(4) == 24
    assert factorial(8) == 40320
    assert factorial(-8) == -40320

run()