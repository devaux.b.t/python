import string
def get_letter_count(word):
       # Votre code ici
       count=0
       liste=[]
       word = word.lower()
       for lettre in string.ascii_lowercase:
              liste.append(lettre)
       for lettre in word:
              if (lettre in liste):
                     count+=1
       return count

def run():
   assert get_letter_count("Oui") == 3
   assert get_letter_count("Bonjour") == 7
   assert get_letter_count("") == 0
   assert get_letter_count(".........hein???") == 4
   assert get_letter_count("Attention y'a quatre mots !") == 21

